package com.rest.assured;

public class Resources {

	public static String getPlacePostDataJson(){
        String res_json = "maps/api/place/add/json";
        return res_json;
    }

	public static String getPlacePostDataXml(){
         String res_json = "maps/api/place/add/xml";
         return res_json;
     }

	public static String getAddBook(){
		 String res = "Library/Addbook.php";
		 return res;
	 }

	public static String getDeleteBook(){
		 String res = "Library/DeleteBook.php";
		 return res;
	 }
	public static String getNearbySearchJson(){
         return ("maps/api/place/nearbysearch/json");
     }

	public static String getLoginToJira(){
		String res ="rest/auth/1/session";
     	return res;
	 }

	public static String getCreateIssueInJira(){
		String res ="rest/api/2/issue";
		return res;
	}

	public static String getAddCommentToIssueInJira(String issueId){
		String res =String.format("rest/api/2/issue/%s/comment",issueId);
		return res;
	}

	public static String getUpdateCommentInIssueInJira(String issueId, String commentId){
		String res =String.format("/rest/api/2/issue/%s/comment/%s", issueId, commentId);
		return res;
	}


}
