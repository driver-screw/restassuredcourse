package com.rest.assured;

import io.restassured.RestAssured;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.junit.Assert;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.sessionId;

public class CommonFunctions {
	public static Properties prop;
	private static Logger log = Logger.getLogger(CommonFunctions.class);
//	private static Response response;
	static{
		prop = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("src/main/resources/env.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    static public XmlPath rawToXml(Response response){
        XmlPath xmlPath = new XmlPath(response.asString());
        return xmlPath;
    }

    static public JsonPath rawToJson(Response response){
        JsonPath jsonPath = new JsonPath(response.asString());
        return jsonPath;
    }

//    static public <T> T rawTo(Response response, Class<T> type){
//        T path =  type.
//        return path;
//    }

    static public Response deleteBook(String id) throws IOException {
        Response response = given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
                .header("Content-Type", "application/json")
                .body(PayLoad.getDeleteBookJson(id))
                .when()
                .post(Resources.getDeleteBook())
                .then()
                .assertThat().statusCode(200)
                .extract().response();
        return response;
    }


	public static  String createSession() throws IOException {
		/** Creating session*/
		log.info("Creating session:");
		RestAssured.baseURI = prop.getProperty("HOST_JIRA");
		Response response = given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
				.header("Content-Type", "application/json")
				.body(PayLoad.getPostDataJson("src/main/resources/res/bodies/jira/log-in-body.json"))
				.when()
				.post(Resources.getLoginToJira())
				.then()
				.assertThat().statusCode(200)
				.extract().response();

		JsonPath js = CommonFunctions.rawToJson(response);
		sessionId = js.get("session.value");
		Assert.assertNotNull("SessionId should not be null", sessionId);
		log.info("SessionId = " + sessionId);
		return sessionId;
	}

	public static  Response createGeneralBug() throws IOException {

    	if (null==sessionId){
			createSession();
		}

		RestAssured.baseURI = prop.getProperty("HOST_JIRA");
		Response response = given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
				.header("Content-Type", "application/json")
				.header("Coockie", "JSESSIONID="+sessionId)
				.body(PayLoad.getPostDataJson("src/main/resources/res/bodies/jira/create-issue.json"))
				.when()
				.post(Resources.getCreateIssueInJira())
				.then()
				.assertThat().statusCode(201)
				.extract().response();

		JsonPath js = CommonFunctions.rawToJson(response);
		String issueId = js.get("id");
		String issueKey = js.get("key");
		Assert.assertNotNull("issueId should not be null", issueId);
		Assert.assertNotNull("issueKey should not be null", issueKey);
		return response;
	}


}
