package com.rest.assured;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class PayLoad {
     static String getPostDataJson(){
        String body = "{" +
                "\"location\": {" +
                "\"lat\": -33.8669710," +
                "\"lng\": 151.1958750" +
                "}," +
                "\"accuracy\": 50," +
                "\"name\": \"Google Shoes!\"," +
                "\"phone_number\": \"(02) 9374 4000\"," +
                "\"address\": \"48 Pirrama Road, Pyrmont, NSW 2009, Australia\"," +
                "\"types\": [\"shoe_store\"]," +
                "\"website\": \"http://www.google.com.au/\"," +
                "\"language\": \"en-AU\"" +
                "}";

        return body;
    }

    static String getPostDataXml(String path) throws IOException {
         return new String(Files.readAllBytes(Paths.get(path)));
    }

	public static String getPostDataJson(String path) throws IOException {
		return getPostDataXml(path);
	}

	static String getPostDataJson(String path, String isbn, String aisle) throws IOException {
		String data =  getPostDataJson(path);
		return String.format(data,"Default Name",isbn,aisle,"Default Author");
	}

    static String getDeleteBookJson(String id) throws IOException {
        String data =  getPostDataJson("src/main/resources/bodies/delete-book-json.json");
        return String.format(data,id);
    }

}
