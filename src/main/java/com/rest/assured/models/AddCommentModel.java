package com.rest.assured.models;

public class AddCommentModel {
    String body;
    Visibility visibility;

   public AddCommentModel(){
        this.body = "This message is created using JsonMapper";
        this.visibility = new Visibility();
    }


    public void setBody(String body) {
        this.body = body;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }

    public String getBody() {
        return body;
    }

    public Visibility getVisibility() {
        return visibility;
    }


}
