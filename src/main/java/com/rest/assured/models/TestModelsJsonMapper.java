package com.rest.assured.models;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class TestModelsJsonMapper {

    public  String filePath = "target/addComment.json";
    private static Logger log = Logger.getLogger(TestModelsJsonMapper.class);
    public File jsonFile;

    @Test
    public void test() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        AddCommentModel addComment = new AddCommentModel();
//        addComment.body = "Some body";
//        Visibility visibility = new Visibility() ;
//        visibility.type = "TYPE";
//        visibility.value = "VALUE";
        jsonFile = new File(filePath);
        objectMapper.writeValue(jsonFile, addComment);

        String addCommentAsString = objectMapper.writeValueAsString(addComment);
        log.info(addCommentAsString);

        AddCommentModel addCommentJsonFromFile = objectMapper.readValue(jsonFile, AddCommentModel.class);

        JsonNode jsonNode = objectMapper.readTree(addCommentAsString);
        String body = jsonNode.get("body").asText();
        log.info("Read body from text: " + body);

        Map<String, Object> map
                = objectMapper.readValue(addCommentAsString, new TypeReference<Map<String,Object>>(){});


        int i = 0;

    }

    @After
    public void cleanUp(){
        jsonFile.delete();
    }

}
