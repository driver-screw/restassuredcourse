package com.rest.assured;

import io.restassured.RestAssured;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import org.junit.Test;

import java.util.Arrays;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


public class DemoTest {

    @Test
    public  void firstTest() {

        RestAssured.baseURI = "http://216.10.245.166";
        given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
//                .param("location","-33.8670522,151.1957362")
//                .param("radius","100")
//                .param("types","food")
//                .param("name","harbour")
//                .param("key","AIzaSyBV8aX_4pOBGl2S77cQ2yp_oWITAsrvB3g")
                .queryParam("key", "qaclick123")
                .body("{"+
                        "\"location\": {"+
                        "\"lat\": -33.8669710,"+
                        "\"lng\": 151.1958750"+
                        "},"+
                        "\"accuracy\": 50,"+
                        "\"name\": \"Google Shoes!\","+
                        "\"phone_number\": \"(02) 9374 4000\","+
                        "\"address\": \"48 Pirrama Road, Pyrmont, NSW 2009, Australia\","+
                        "\"types\": [\"shoe_store\"],"+
                        "\"website\": \"http://www.google.com.au/\","+
                        "\"language\": \"en-AU\""+
                        "}")
                .when()
//                    .get("maps/api/place/nearbysearch/json")
                        .post("maps/api/place/add/json")
                .then()
                .assertThat().statusCode(200)
                    .contentType(ContentType.JSON)
                .body("result[0].geometry.location.lat",equalTo("-33.8706756"))
        ;

    }
}
