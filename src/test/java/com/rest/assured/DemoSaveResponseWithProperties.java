package com.rest.assured;

import io.restassured.RestAssured;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class DemoSaveResponseWithProperties {
    private static Logger log = Logger.getLogger(DemoSaveResponseWithProperties.class);
    private Properties prop;
    private Response response;

    @Before
    public void getData() throws IOException {
        prop = new Properties();
        FileInputStream fis = new FileInputStream("src/main/resources/env.properties");
        prop.load(fis);

    }

    @Test
    public void deletePlace() {
        postPlace();
        log.info("Response as String:\n" + response.asString());
        JsonPath js = new JsonPath(response.asString());
        String placeId = js.get("place_id");
        log.info("placeId = " + placeId);

        given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
                .queryParam("key", prop.getProperty("API_KEY"))
                .body(String.format("{\"place_id\": \"%s\"}", placeId))
                .when()
                .post(prop.getProperty("DELETE_URI"))
                .then()
                .assertThat().statusCode(200)
                .contentType(ContentType.JSON).and()
                .body("status", equalTo("OK"));
    }


    private void postPlace() {
        RestAssured.baseURI = prop.getProperty("HOST");
        response = given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
                .queryParam("key", prop.getProperty("API_KEY"))
                .body(PayLoad.getPostDataJson())
                .when()
                .post(Resources.getPlacePostDataJson())
                .then()
                .assertThat().statusCode(200)
                .contentType(ContentType.JSON)
                .extract().response();
    }
}
