package com.rest.assured;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Rule;
import org.junit.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static io.restassured.RestAssured.given;

public class DemoWithWireMock {
    String URL = "http://localhost:8080";
    String RESOURCE = "maps/api/place/nearbysearch/json";
//    private static org.apache.log4j.Logger log = Logger.getLogger(DemoWithWireMock.class);

    @Rule
    public WireMockRule wireMockRule = new WireMockRule();

    @Test
    public void Test(){
        stubFor(get(urlPathEqualTo("/"+RESOURCE))
//                .withHeader("Accept", equalTo("text/xml"))
//                .withHeader("location",containing("-33.8670522,151.1957362"))
                .willReturn(aResponse()
                        .withStatus(401)
                        .withHeader("Content-Type", "application/json")
                        .withBody("<response>Some content</response>")));

        RestAssured.baseURI = URL;
        given()
                .param("location","-33.8670522,151.1957362")
                .param("radius","100")
                .param("types","food")
                .param("name","harbour")
                .param("key","AIzaSyBV8aX_4pOBGl2S77cQ2yp_oWITAsrvB3g")
                .queryParam("key","value")
        .when()
                .get(RESOURCE)
        .then().assertThat().statusCode(401)
                .contentType(ContentType.JSON);
    }
}
