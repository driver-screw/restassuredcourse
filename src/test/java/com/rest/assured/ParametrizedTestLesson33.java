package com.rest.assured;

import io.restassured.RestAssured;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;

import static io.restassured.RestAssured.given;

@RunWith(Parameterized.class)
public class ParametrizedTestLesson33 {
    private static Logger log = Logger.getLogger(DemoSaveResponseWithPropertiesWithXml.class);
    private Properties prop;
    private Response response;
    String bookId;

    @Parameterized.Parameters( name = "{index}: isbn = {0}, aisle= {1} ")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { "isbn_1", "aisle_1" }, { "isbn_2", "aisle_2" }
        });
    }

    private String fIsbn;

    private String fAisle;

    public ParametrizedTestLesson33(String  isbn, String aisle) {
        this.fIsbn = isbn;
        this.fAisle = aisle;
    }

    @Before
    public void getData() throws IOException {
        log.info("--------------------------BEFORE-------------------------" +"\n\r");
        prop = new Properties();
        FileInputStream fis = new FileInputStream("src/main/resources/env.properties");
        prop.load(fis);

    }


    @Test
    public void addBook() throws IOException {
        log.info("--------------------------TEST-------------------------");
        RestAssured.baseURI = prop.getProperty("HOST");
        response = given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
                .header("Content-Type", "application/json")
                .body(PayLoad.getPostDataJson("src/main/resources/bodies/add-book--with-placeholders-json.json"
                        , fIsbn, fAisle))
                .when()
                .post(Resources.getAddBook())
                .then()
                .assertThat().statusCode(200)
                .extract().response();

        JsonPath js = CommonFunctions.rawToJson(response);
        bookId = js.get("ID");
        Assert.assertNotNull("ID should not be null", bookId);
        log.info("ID = " + bookId);

    }

    @After
    public void cleanUp() throws IOException {
        log.info("--------------------------AFTER-------------------------");
//        com.rest.assured.CommonFunctions.deleteBook(bookId);
    }

}
