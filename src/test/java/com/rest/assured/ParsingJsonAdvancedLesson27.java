package com.rest.assured;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class ParsingJsonAdvancedLesson27 {
    String RESOURCE = Resources.getNearbySearchJson();
//    private static org.apache.log4j.Logger log = Logger.getLogger(com.rest.assured.ParsingJsonAdvancedLesson27.class);
    private static Logger log = LoggerFactory.getLogger(ParsingJsonAdvancedLesson27.class);

    private Properties prop;
    private Response response;

    @Before
    public void getData() throws IOException {
        prop = new Properties();
        FileInputStream fis = new FileInputStream("src/main/resources/env.properties");
        prop.load(fis);

    }

    @Test
    public void Test(){

        RestAssured.baseURI = prop.getProperty("HOST_GOOGLE");
        response = given().log().all()
                .param("location","-33.8670522,151.1957362")
                .param("radius","500")
                .param("types","food")
                .param("name","harbour")
                .queryParam("key",prop.getProperty("API_KEY_GOOGLE"))
        .when()
                .get(RESOURCE)
        .then().log().all().and().assertThat().statusCode(200)
                .contentType(ContentType.JSON).extract().response();
//        log.info("Log info: response = \n" + response.asString());

        JsonPath jsonPath = CommonFunctions.rawToJson(response);
        int count = jsonPath.get("results.size()");
//        int[] array = new int[20];
//        array[5] = count;
        for ( int i=0; i<count; i++) {
            String name = jsonPath.get(String.format("results[%d].name",i) );
            log.info(String.format("Name #%d is: %s", i,name));
        }

    }
}
