package com.rest.assured;

import io.restassured.RestAssured;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class excelDrivenLesson31 {
    private static Logger log = Logger.getLogger(DemoSaveResponseWithPropertiesWithXml.class);
    private Properties prop;
    private Response response;

    @Before
    public void getData() throws IOException {
        prop = new Properties();
        FileInputStream fis = new FileInputStream("src/main/resources/env.properties");
        prop.load(fis);

    }


    @Test
    public void addBook() throws IOException {
        RestAssured.baseURI = prop.getProperty("HOST");
        response = given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
                .header("Content-Type", "application/json")
                .body(PayLoad.getPostDataJson("src/main/resources/bodies/add-book--with-placeholders-json.json"
                        , "1234324", "ufejvskdhf"))
                .when()
                .post(Resources.getAddBook())
                .then()
                .assertThat().statusCode(200)
                .extract().response();

        JsonPath js = CommonFunctions.rawToJson(response);
        String id = js.get("ID");
        Assert.assertNotNull("ID should not be null", id);
        log.info("ID = " + id);

    }

    @After
    public void cleanUp() {

    }

}
