package com.rest.assured.jiraAutomating;

import com.rest.assured.CommonFunctions;
import com.rest.assured.PayLoad;
import com.rest.assured.Resources;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import static com.rest.assured.CommonFunctions.createGeneralBug;
import static com.rest.assured.CommonFunctions.createSession;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.sessionId;

public class Lesson43AddCommentToIssue {

	private static Logger log = Logger.getLogger(Lesson43AddCommentToIssue.class);
	private Properties prop;
//	private Response response;
	private String issueId;

	@Before
	public void getDataAndCreateGeneralBug() throws IOException {
		log.info("--------------------------BEFORE-------------------------" +"\n\r");
		prop = new Properties();
		FileInputStream fis = new FileInputStream("src/main/resources/env.properties");
		prop.load(fis);
		createSession();
		Response response = createGeneralBug();
		JsonPath js = CommonFunctions.rawToJson(response);
		issueId = js.get("id");
	}

	@Test
	public void testJiraApi() throws IOException {
		log.info("--------------------------TEST-------------------------");

		log.info("Adding comment");
		Response addCommentResponse = given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
				.header("Content-Type", "application/json")
				.header("Coockie", "JSESSIONID="+sessionId)
				.body(PayLoad.getPostDataJson("src/main/resources/res/bodies/jira/add-comment-to-issue.json"))
				.when()
				.post(Resources.getAddCommentToIssueInJira(issueId))
				.then()
				.assertThat().statusCode(201)
				.extract().response();

		JsonPath jsonPathAddCommentResponse = CommonFunctions.rawToJson(addCommentResponse);
		String commentId = jsonPathAddCommentResponse.get("id");

		log.info("Updating comment");
		Response updateCommentResponse = given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
				.header("Content-Type", "application/json")
				.header("Coockie", "JSESSIONID="+sessionId)
				.body(PayLoad.getPostDataJson("src/main/resources/res/bodies/jira/add-comment-to-issue.json"))
				.when()
				.put(Resources.getUpdateCommentInIssueInJira(issueId, commentId))
				.then()
				.assertThat().statusCode(201)
				.extract().response();

	}

}
