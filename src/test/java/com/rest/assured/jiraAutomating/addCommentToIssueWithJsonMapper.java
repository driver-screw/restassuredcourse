package com.rest.assured.jiraAutomating;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rest.assured.CommonFunctions;
import com.rest.assured.Resources;
import com.rest.assured.models.AddCommentModel;
import com.rest.assured.models.CreateIssueResponse;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import static com.rest.assured.CommonFunctions.createGeneralBug;
import static com.rest.assured.CommonFunctions.createSession;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.sessionId;

public class addCommentToIssueWithJsonMapper {

	private static Logger log = Logger.getLogger(addCommentToIssueWithJsonMapper.class);
	private Properties prop;
//	private Response response;
	private String issueId;

	@Before
	public void getDataAndCreateGeneralBug() throws IOException {
		log.info("--------------------------BEFORE-------------------------" +"\n\r");
		prop = new Properties();
		FileInputStream fis = new FileInputStream("src/main/resources/env.properties");
		prop.load(fis);
		createSession();
		Response response = createGeneralBug();
		CreateIssueResponse createIssueResponse = response.as(CreateIssueResponse.class);
		JsonPath js = CommonFunctions.rawToJson(response);
		issueId = js.get("id");
	}

	@Test
	public void testJiraApi() throws IOException {
		log.info("--------------------------TEST-------------------------");

		ObjectMapper objectMapper = new ObjectMapper();
		AddCommentModel addComment = new AddCommentModel();
		log.info("Adding comment");
		Response addCommentResponse = given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
				.header("Content-Type", "application/json")
				.header("Coockie", "JSESSIONID="+sessionId)
				.body(addComment)
				.when()
				.post(Resources.getAddCommentToIssueInJira(issueId))
				.then()
				.assertThat().statusCode(201)
				.extract().response();

		JsonPath jsonPathAddCommentResponse = CommonFunctions.rawToJson(addCommentResponse);
		String commentId = jsonPathAddCommentResponse.get("id");
	}

}
