package com.rest.assured.jiraAutomating;

import com.rest.assured.CommonFunctions;
import com.rest.assured.PayLoad;
import com.rest.assured.Resources;
import io.restassured.RestAssured;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import static com.rest.assured.CommonFunctions.createGeneralBug;
import static com.rest.assured.CommonFunctions.createSession;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.sessionId;

public class Lesson42 {

	private static Logger log = Logger.getLogger(Lesson42.class);
	private Properties prop;
	private Response response;

	@Before
	public void getData() throws IOException {
		log.info("--------------------------BEFORE-------------------------" +"\n\r");
		prop = new Properties();
		FileInputStream fis = new FileInputStream("src/main/resources/env.properties");
		prop.load(fis);

	}

	@Test
	public void testJiraApi() throws IOException {
		log.info("--------------------------TEST-------------------------");
		createSession();
		response = createGeneralBug();

		JsonPath js = CommonFunctions.rawToJson(response);
		String issueId = js.get("id");
		String issueKey = js.get("key");
		Assert.assertNotNull("issueId should not be null", issueId);
		Assert.assertNotNull("issueKey should not be null", issueKey);
		log.info("issueKey = " + issueKey);
	}

}
