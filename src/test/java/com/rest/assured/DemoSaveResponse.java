package com.rest.assured;

import io.restassured.RestAssured;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.Arrays;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class DemoSaveResponse {
    private static org.apache.log4j.Logger log = Logger.getLogger(DemoSaveResponse.class);

    private Response response;

    @Test
    public void deletePlace() {
        postPlace();
        log.info("Response as String:\n" + response.asString());
        JsonPath js = new JsonPath(response.asString());
        String placeId = js.get("place_id");
        log.info("placeId = " + placeId);

        given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
                .queryParam("key", "qaclick123")
                .body(String.format("{\"place_id\": \"%s\"}", placeId))
                .when()
                .post("maps/api/place/delete/json")
                .then()
                .assertThat().statusCode(200)
                .contentType(ContentType.JSON).and()
                .body("status", equalTo("OK"));
    }


    private void postPlace() {
        RestAssured.baseURI = "http://216.10.245.166";
        response = given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
                .queryParam("key", "qaclick123")
                .body("{" +
                        "\"location\": {" +
                        "\"lat\": -33.8669710," +
                        "\"lng\": 151.1958750" +
                        "}," +
                        "\"accuracy\": 50," +
                        "\"name\": \"Google Shoes!\"," +
                        "\"phone_number\": \"(02) 9374 4000\"," +
                        "\"address\": \"48 Pirrama Road, Pyrmont, NSW 2009, Australia\"," +
                        "\"types\": [\"shoe_store\"]," +
                        "\"website\": \"http://www.google.com.au/\"," +
                        "\"language\": \"en-AU\"" +
                        "}")
                .when()
                .post("maps/api/place/add/json")
                .then()
                .assertThat().statusCode(200)
                .contentType(ContentType.JSON)
                .extract().response();
    }
}
